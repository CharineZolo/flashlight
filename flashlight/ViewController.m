//
//  ViewController.m
//  flashlight
//
//  Created by 米拉 on 2/28/17.
//  Copyright © 2017 米拉. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray * imageArray;
    NSMutableArray * textArray;
}

@property (strong, nonatomic) IBOutlet UITableView *table;
@end

@implementation ViewController{
    NSMutableArray *tableData;
}

@synthesize Textbox;
@synthesize displayB;
@synthesize send,stop;
@synthesize chattableview;
static bool haltBlinking = true;

- (void)viewDidLoad {
    
  
    [super viewDidLoad];
   // self->textArray = [[[NSMutableArray alloc] init] autorelease];
    Textbox.text=@"";
    [self arraySetup];
}

- (void) arraySetup{
    imageArray = [NSMutableArray arrayWithArray:@[@"flash"]];
    //textArray = [NSMutableArray arrayWithArray:@""];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return imageArray.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellId = @"cell";
    UITableViewCell * cell  = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if(cell == nil){
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:cellId];
    }
    NSString *Text = [Textbox text];

    cell.imageView.image = [UIImage imageNamed:imageArray[indexPath.row ]];
    cell.textLabel.text = Text;
    if(Text != nil){
        cell.detailTextLabel.text= @"Delivered";

    }
    else {
        cell.detailTextLabel.text= @"";

    }
     Textbox.text=@"";
    return cell;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //NSLog(@"hi");
    [textField resignFirstResponder];
    self.Textbox.delegate = self;

    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) handlelight:(char) t
{
    int binary= (int) t;
    int i=0,mark=0;
    AVCaptureDevice * captDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];

    if ([captDevice hasFlash]&&[captDevice hasTorch]) {
        
        while(i<8&&haltBlinking){
            
            mark=binary & 128;
            binary=binary<<1;
            
            i++;
            NSLog(@"%d",mark);
            
            // mark=1;
            if(mark==128){
                self.displayB.text=@"1";
                displayB.tag=1;
                
                [captDevice lockForConfiguration:nil];
                [captDevice setTorchMode:AVCaptureTorchModeOn];
                [captDevice unlockForConfiguration];
               sleep(1);
               // usleep(900);
     
            }
            
            else{
                displayB.tag=0;
                self.displayB.text=@"0";

                [captDevice lockForConfiguration:nil];
                //[captDevice setTorchMode:AVCaptureTorchModeOff];
                [captDevice setTorchMode:AVCaptureTorchModeOn];

                [captDevice unlockForConfiguration];
                //sleep(1);

                 usleep(500);
                
            }
            [captDevice lockForConfiguration:nil];
            [captDevice setTorchMode:AVCaptureTorchModeOff];
            //[captDevice setTorchMode:AVCaptureTorchModeOn];
            
            [captDevice unlockForConfiguration];
          
            [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
            sleep(2);

           // usleep(2000);
       
        }
   
    }
    //================end===============================
    if(!haltBlinking)    self.displayB.text=@"0";

    [captDevice lockForConfiguration:nil];
    [captDevice setTorchMode:AVCaptureTorchModeOff];
    [captDevice unlockForConfiguration];
    
    //Text=[Textbox text];
    //[textArray addObject:Text];
   // [imageArray addObject:@"flash"];
    [self.table reloadData];
   
    

}

- (IBAction)PressSend:(id)sender {
    //self.displayB.text=@"hi";
    
    haltBlinking = true;
    self.displayB.text=@"0";

   NSString * Text = [Textbox text];
    int j=0;
    const char *c = [Text UTF8String];
    while(j<[Text length]&&haltBlinking){
        NSLog(@"%c",c[j]);
        [self handlelight:c[j]];
       j++;
        
    }
    [tableData addObject: Text];
    
    //static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [chattableview dequeueReusableCellWithIdentifier:Text];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Text];
    }
    
    //cell.textLabel.text = [tableData objectAtIndex:indexPath.row];

  }


-(IBAction)stopBlinking:(id)sender
{
    haltBlinking = false;
    
}


@end
